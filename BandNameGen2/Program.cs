﻿// todo
// make a batch-version, so when called it can generate a file with 10 band names
// try and integrate other formats - Maybe merging words into new ones (at least one vocal)
// add a .txt with names which can be used together with pronouns(?)
// consider adding a "song name generator" alongside the band-name one
using System;

namespace BandNameGen2
{
    class Program
    {
        static void Main(string[] args)
        {
            // turn on debugging
            bool debugging = false;
            //

            Console.WriteLine("Welcome to random bandname generator");
            Console.WriteLine("press any key to start the generator");
            Console.ReadKey();

            string[] Nouns = System.IO.File.ReadAllLines(@"Nouns.txt");
            string[] Adjectives = System.IO.File.ReadAllLines(@"Adjectives.txt");

            int AllThemNouns = Nouns.Length;
            int AllThemAdjectives = Adjectives.Length;

            Random Noun = new Random();
            Random Adjective = new Random();
            char happy;

            do
            {
                // generate new numbers for fetching nouns and adjectives
                int genrand = Noun.Next(0, AllThemNouns);
                int genrand2 = Adjective.Next(0, AllThemAdjectives);

                if (debugging == true)
                {
                    Console.WriteLine("Noun# " + genrand);
                    Console.WriteLine("Adjective# " + genrand2);
                }

                // select noun and adjective based on random generated number
                string selectedNoun = Nouns[genrand];
                string selectedAdjective = Adjectives[genrand2];

                Console.WriteLine("The " + selectedAdjective + " " + selectedNoun);

                // ask if user is happy with result

                Console.WriteLine("Are you happy with the name? Press 'y' to exit");
                happy = Console.ReadKey().KeyChar;
                if (happy is 'y')
                {
                    Console.WriteLine("Rock on! Closing...");
                }
                else
                {
                    Console.WriteLine("Composing something new...");
                }
            }
            while (happy != 'y');
        }
    }
}
